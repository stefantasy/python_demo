#!/usr/bin/env python 
# -*- coding: utf-8 -*-
from math import pi
from geometry_msgs.msg import Pose
from tf.transformations import quaternion_from_euler
def e_to_q(x,y,th):#输入x（前）坐标，y（左）坐标，th（平面朝向0～360度）
    new_pose=Pose()
    new_pose.position.x=x
    new_pose.position.y=y
    #机器朝向，平面朝向弧度转化成四元数空间位姿
    q=quaternion_from_euler(0.0,0.0,th/180.0*pi)
    new_pose.orientation.x=q[0]
    new_pose.orientation.y=q[1]
    new_pose.orientation.z=q[2]
    new_pose.orientation.w=q[3]
    return  new_pose
print(e_to_q(0,0,0))









       


