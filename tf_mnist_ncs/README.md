# 使用tensorflow训练一个简单的神经网络（mnist数据集）用来识别手写数字,分为三个步骤，训练，模型转换，加载使用。
## 一、模型训练
* python版本 python3.5.2
* tensorflow 版本 1.4.0
* 开始训练，这个过程会自动从网上获取数据集，并训练，默认训练次数是20000次
    
    $:python3 mnist_deep_mod.py 
* 20000次训练大约需要20分钟，会生成三个模型文件 

    mnist_model.meta 
    mnist_model.index
    mnist_model.data-00000-of-00001


##  二、模型文件转换
* 把模型文件转换成可以编译程NCS的模型文件
    
    $:python3 mnist_deep_inference.py   
* 会生成三个模型文件
 
    mnist_inference.meta
    mnist_inference.index
    mnist_inference.graph
 * 把新生成的模型文件转换成MOVIDIUS可加载的graph文件（具体操作参考https://movidius.github.io/ncsdk/tools/compile.html）
 
    $:mvNCCompile mnist_inference.meta -s 12 -in input -on output -o mnist_inference.graph
* 生成graph文件 mnist_inference.graph
## 三、模型的加载及使用（需要插入movidius）
* 该过程会调用opencv 从画面中捕捉画面，加载并使用NCS。来加速数字识别
    
    $:python3 ncsdk_mnist_opencv.py

