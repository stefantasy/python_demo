import tensorflow as tf
import matplotlib.pyplot as plt
import PIL.Image as Image
import numpy as np
from tensorflow.contrib.learn.python.learn.datasets.mnist import read_data_sets
 
mnist = read_data_sets("data/",one_hot=True)
 
print(mnist)
print(mnist.train.images.shape,mnist.train.labels.shape)  #(55000, 784),(55000, 10)
print(mnist.validation.images.shape,mnist.validation.labels.shape)  #(5000,784),(5000,10)
print(mnist.test.images.shape,mnist.test.labels.shape)   #(10000, 784),(10000, 10)
 
x = tf.placeholder(dtype='float',shape=[None,784])
w = tf.Variable(tf.zeros([784,10]))
b = tf.Variable(tf.zeros([10]))
 
y = tf.nn.softmax(tf.matmul(x,w)+b)
 
#定义实际label占位符
y_ = tf.placeholder(dtype='float',shape=[None,10])
#交叉熵
cross_entropy =  -tf.reduce_sum(y_ * tf.log(y))
train_step = tf.train.GradientDescentOptimizer(learning_rate=0.01).minimize(cross_entropy)
 
init = tf.initialize_all_variables()
 
sess = tf.Session()
sess.run(init)
 
step = 500     #迭代次数
loss_list = [] #保存loss，便于画图
for i in range(step):
    batch_xs,batch_ys = mnist.train.next_batch(100)  #shape: (100, 784) (100, 10)
    _,loss,weight= sess.run([train_step,cross_entropy,w],feed_dict={x:batch_xs,y_:batch_ys})
    loss_list.append(loss)
print('weight\'s shape:',weight.shape)   #（784,10)
#===========================================
#显示weight所表征的图，即学到的东西
img_size = 28
for col in range(10):
    singel_img = weight[:,col].reshape(img_size,img_size) #取出每列，即数字0~9代表的数
    singel_img = [255 if singel_img[i,j]>0 else 0 for i in range(img_size) for j in range(img_size)] #作转换，正的为255，负的为0，类型为list
    singel_img = np.array(singel_img).reshape(img_size,img_size)   #将list reshape成原图大小（28,28）
    img = Image.fromarray(np.uint8(singel_img)).convert('RGB')                #数据类型转成uint8，并转成RGB模式
    img.save('img/'+'digit_%d.jpg'%col)                         #保存图片
    #plt.imshow(img)                                            #显示图片
    #plt.show()
#===========================================
#评估模型
correct_prediction = tf.equal(tf.argmax(y,1),tf.argmax(y_,1)) #由于标签向量是由0,1组成，因此最大值1所在的索引位置就是类别标签
#correct_prediction是bool类型
accuracy = tf.reduce_mean(tf.cast(correct_prediction,'float'))  #我们可以把布尔值转换成浮点数，然后取平均值。例如，[True, False, True, True] 会变成 [1,0,1,1] ，取平均值后得到 0.75
 
print('[accuracy,loss]:',sess.run([accuracy,cross_entropy],feed_dict={x:mnist.test.images,y_:mnist.test.labels}))
plt.figure(1)
plt.plot(range(1,step+1),loss_list,'o-',ms=3,lw=1,color='black')
plt.show()