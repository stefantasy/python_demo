#!/usr/bin/env python
# _*_ coding:utf-8 _*_
import cv2
import rospy
import dlib
from sensor_msgs.msg import RegionOfInterest as ROI
from sensor_msgs.msg import Image

PREDICTOR_PATH = "shape_predictor_68_face_landmarks.dat"

# 使用dlib自带的frontal_face_detector作为人脸提取器
detector = dlib.get_frontal_face_detector()

# 使用官方模型构建特征提取器
predictor = dlib.shape_predictor(PREDICTOR_PATH)
rospy.init_node('face_roi_node', anonymous=False)
rate = rospy.Rate(10)
#框住人脸的矩形边框颜色
color = (0, 255, 0)

#捕获指定摄像头的实时视频流
cap = cv2.VideoCapture(0)
roipub = rospy.Publisher('roi', ROI, queue_size=10)
def face_find():
        _, frame = cap.read()   #读取一帧视频
        # 图像灰化，降低计算复杂度
        frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        small_frame = cv2.resize(frame_gray, (0, 0), fx=0.5, fy=0.5)
        # 使用detector进行人脸检测 rects为返回的结果
        rects = detector(small_frame, 1)
        if len(rects) > 0:
            for k, d in enumerate(rects):
                roi_data=ROI()
                roi_data.x_offset=abs((d.left()*2))
                roi_data.y_offset=abs((d.top()*2))
                roi_data.height=abs((d.bottom()*2)-d.top()*2)
                roi_data.width=abs((d.right()*2-d.left()*2))
                roi_data.do_rectify=0
                cv2.rectangle(frame, (d.left()*2, d.top()*2), (d.right()*2, d.bottom()*2), (0, 255, 0))
                shape = predictor(small_frame, d)
                for i in range(68):
                    cv2.circle(frame, (shape.part(i).x*2, shape.part(i).y*2), 2, (0, 255, 0), -1, 8)
                roipub.publish(roi_data)
                rate.sleep()            
        cv2.imshow("find face", frame)                   
        #等待10毫秒看是否有按键输入
        k = cv2.waitKey(10)
        #如果输入q则退出循环
        if k & 0xFF == ord('q'):
            # break
            cap.release()
            cv2.destroyAllWindows()
def input_img()

if __name__ == '__main__':
	try:
	    while not rospy.is_shutdown():
		face_find()
	except:
	    cap.release()
	    cv2.destroyAllWindows()

