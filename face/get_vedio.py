import cv2 
from cv_bridge import CvBridge
import rospy
from sensor_msgs.msg import Image
cvb=CvBridge()

def callback(data):
    img_frame=cvb.imgmsg_to_cv2(data,desired_encoding="passthrough")
    print(img_frame)
    cv2.imshow("find face", img_frame) 
    cv2.waitKey(1)

def get_img():
    rospy.init_node('listener', anonymous=True)
    rospy.Subscriber("image_raw", Image, callback)
    rospy.spin()
if __name__ == '__main__':
    get_img()
