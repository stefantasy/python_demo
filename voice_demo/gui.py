# -*- coding: utf-8 -*-
 
"""
PyQt5 tutorial 
 
This example shows how to use
a QComboBox widget.
 
author: py40.com
last edited: 2017年3月
"""
from aip import AipSpeech
import sys
import os
from PyQt5.QtWidgets import (QWidget, QLabel,
                             QComboBox, QApplication,QPushButton,
                             QLineEdit,QSizePolicy,QMainWindow)
from PyQt5.QtCore import QCoreApplication,QSize
from PyQt5.QtGui import QIcon
from PyQt5 import QtGui,QtCore


 
class voice_mix(QWidget):
    personvoice = {'男1':1, '男2':3, '女1':0, '女2':4}
    per = '男1'
    pitch = 0
    speed = 5
    voice_text = "你好"
    # '你好，我是AI智能教育机器人，卡丁～'
    def __init__(self):
        super().__init__()
        # self.per = 0
        self.initUI()
 
    def initUI(self):
        # ************下拉框标签*********
        lbl = QLabel(self)
        lbl.move(50, 80)
        lbl.setText('发音人')
        lbl = QLabel(self)
        lbl.move(150, 80)
        lbl.setText('音调')
        lbl = QLabel(self)
        lbl.move(250, 80)
        lbl.setText('语速')


        # ***********quit button**********
        qbtn = QPushButton('Quit', self)
        qbtn.clicked.connect(QCoreApplication.instance().quit)
        qbtn.resize(qbtn.sizeHint())
        qbtn.move(500, 270)  

        # ******************下拉框created here****************
        people = ('男1', '男2', '女1', '女2')
        perbox = QComboBox(self)
        self.create_drag_down(perbox,people,50,100)
        perbox.activated[str].connect(self.set_person)
        pitches = list('0123456789')
        pitbox = QComboBox(self)
        self.create_drag_down(pitbox,pitches,150,100)
        pitbox.activated[str].connect(self.set_pitch)

        speeds = list('0123456789')
        pitbox = QComboBox(self)
        self.create_drag_down(pitbox,speeds,250,100)
        pitbox.activated[str].connect(self.set_spd)

        # ************文本框以及其标签****************
        self.label = QLabel(self)
        self.label.move(150, 20)
        self.label.setText('请在下方输入内容:')
        qle = QLineEdit(self)
        qle.move(150, 40)
        qle.setFixedSize(200,30)
        qle.textChanged[str].connect(self.set_text)

        # **************************** 确认并输出*******************
        path1 = os.getcwd()
        playicon = QIcon(path1+"/voice/playicon.jepg")
        b = QSize(30,30)
        btn = QPushButton(playicon,'Play', self)
        btn.setIconSize(b)
        btn.clicked.connect(lambda: self.confirm(self.personvoice[self.per],self.pitch,self.speed,self.voice_text))
        btn.resize(100,50)
        # print(qbtn.sizeHint())
        btn.move(350, 100)   



        # self.use_palette()
        # app_icon = QIcon("~/voice/icon.jpeg")
        # self.setWindowIcon(app_icon)
        self.setGeometry(600, 600, 600, 300)
        self.setWindowTitle('speech_synthesis')
        self.setObjectName("speech")
        self.setStyleSheet("#speech{background-color: lightblue}") #setting background to lightblue here
        self.show()

        
        # self.setGeometry(300, 300, 250, 150)
        # self.setWindowTitle('Quit button')    
        # self.show()
    def use_palette(self): 
        self.setWindowTitle("设置背景图片") 
        window_pale = QtGui.QPalette() 
        window_pale.setBrush(self.backgroundRole(),  QtGui.QBrush(QtGui.QPixmap("~/voice/icon.jepg"))) 
        self.setPalette(window_pale)
    def create_drag_down(self,name,variables,positionx,positiony):
        name.addItems(variables)
        name.move(positionx, positiony)
        name.setFixedSize(70,40)

    def set_person(self,name):
        self.per = name
    def set_pitch(self,integer):
        self.pitch = int(integer)
    def set_spd(self,spd):
        self.speed = spd
    def set_text(self,text):
        if(text == ''):
            self.label.setText('请在下方输入内容:')
            self.label.adjustSize()
        else:
            self.label.setText(text)
            # self.label.adjustSize()
        self.voice_text = text

    def confirm(self,per,pitch,speed,text):

        # """ 你的 APPID AK SK """
        APP_ID = '16425806'
        API_KEY = 'Lz5VMGB5qufER9kxiik0IR1a'
        SECRET_KEY = 'EgGkUUHWHLtkkYG2GIpAswG9hK7hOzMH'
        client = AipSpeech(APP_ID, API_KEY, SECRET_KEY)


        result  = client.synthesis(text, 'zh', 1, {'vol': 9,'spd': speed,'per':per,'pit':pitch})

        # tex	String	合成的文本，使用UTF-8编码，
        # 请注意文本长度必须小于1024字节	是
        # cuid	String	用户唯一标识，用来区分用户，
        # 填写机器 MAC 地址或 IMEI 码，长度为60以内	否
        # spd	String	语速，取值0-9，默认为5中语速	否
        # pit	String	音调，取值0-9，默认为5中语调	否
        # vol	String	音量，取值0-15，默认为5中音量	否
        # per	String	发音人选择, 0为女声，1为男声，
        # 3为情感合成-度逍遥，4为情感合成-度丫丫，默认为普通女	否


        # 识别正确返回语音二进制 错误则返回dict 参照下面错误码
        if not isinstance(result, dict):
            name = self.per + '.mp3'
            with open(name, 'wb') as f:
                f.write(result) 
                path = os.getcwd()
            print("this is path:", path)
            os.system("play "+ path + "/"+ name)
    
 
if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = voice_mix()
    sys.exit(app.exec_())