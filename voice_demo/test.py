
#!/usr/bin/python3
# -*- coding: utf-8 -*-
 
"""
Py40 PyQt5 tutorial 
 
This program creates a quit
button. When we press the button,
the application terminates. 
 
author: Jan Bodnar
website: py40.com 
last edited: January 2015
"""
 
import sys
from PyQt5.QtWidgets import QWidget, QPushButton, QApplication
from PyQt5.QtCore import QCoreApplication,QTimer
 
 
class Example(QWidget):
    
    def __init__(self):
        super().__init__()
        
        self.initUI()
        
        
    def initUI(self):               
        self.check = True
        self.qbtn = QPushButton('play', self)
        # qbtn.clicked.connect(self.play)
        # self.qbtn.resize(self.qbtn.sizeHint())
        self.qbtn.move(50, 50)  

        self.pbtn = QPushButton('pause', self)
        self.pbtn.clicked.connect(self.run)
        self.pbtn.move(50, 50)    
        self.time = QTimer(self)
        self.time.setInterval(1000)  
        self.time.timeout.connect( self.refresh)
        self.setGeometry(300, 300, 250, 150)
        self.setWindowTitle('test button')    
        self.show()
    def run(self):
        if self.check:
            self.changebtn()
        else:
            self.changeback()
    def changebtn(self):
        self.pbtn.setText('new')
    def changeback(self):
        self.pbtn.setText('pause')

    def play(self):
        print('play')
    def pause(self):
        self.pbtn.move(70, 50)
        # self.qbtn.move(50, 50)
        if self.pbtn.isEnabled():
            print('disable')
            # btn.setEnabled(True)
            self.pbtn.setEnabled(False)
            self.time.start()
            # self.qbtn.move(50, 50) 
    def refresh(self):
        print('enable')
        self.pbtn.setEnabled(True)
        self.time.stop()
                
        
if __name__ == '__main__':
    
    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())