#!/usr/bin/env python
#coding=utf8
import rospy
from geometry_msgs.msg import Pose, PoseWithCovarianceStamped, Point, Quaternion, Twist 
from std_msgs.msg import String
import os
import itchat
p=PoseWithCovarianceStamped()
locations = dict()

@itchat.msg_register(itchat.content.TEXT)
def callback(msg):
	global locations
	msg0 = msg['Text']
	print(msg0)
	if msg0[0:2] == u'标记':
		getpose()
		goal_name=msg0[2:]
		print(goal_name)
		x=p.pose.pose.position.x
		y=p.pose.pose.position.y
		z=p.pose.pose.position.z
		x2=p.pose.pose.orientation.x
		y2=p.pose.pose.orientation.y
		z2=p.pose.pose.orientation.z
		w=p.pose.pose.orientation.w	
		# print(x,y,z,x2,y2,z2,w)	
		locations[goal_name]=Pose(Point(x,y,z),Quaternion(x2,y2,z2,w))
		print(locations)

def getpose():
	rospy.init_node('mark_navigation', anonymous=True)  
	rospy.Subscriber("amcl_pose", PoseWithCovarianceStamped, readpose)
def readpose(data):
	global p
	p=data 	 

if __name__ == '__main__':
    itchat.auto_login(True)
    itchat.run()
	


