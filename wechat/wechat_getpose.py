#!/usr/bin/env python
#coding=utf8
import roslib
import rospy
import actionlib
from actionlib_msgs.msg import * 
from geometry_msgs.msg import Pose, PoseWithCovarianceStamped, Point, Quaternion, Twist 
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal 
from random import sample  
from math import pow, sqrt
import itchat
p=PoseWithCovarianceStamped()
savelocations = dict()


def getpose():
	rospy.Subscriber("amcl_pose", PoseWithCovarianceStamped, readpose)
def readpose(data):
	global p
	p=data
	 

@itchat.msg_register(itchat.content.TEXT)
def callback(msg):
    global savelocations
    msg0 = msg['Text']
	# print(msg0)
    if msg0[0:2] == u'标记':
		getpose()
		goal_name=msg0[2:]
		#print(goal_name)
		x=p.pose.pose.position.x
		y=p.pose.pose.position.y
		z=p.pose.pose.position.z
		ox=p.pose.pose.orientation.x
		oy=p.pose.pose.orientation.y
		oz=p.pose.pose.orientation.z
		ow=p.pose.pose.orientation.w	
		print(x,y,z,ox,oy,oz,ow)	
		savelocations[goal_name]=Pose(Point(x,y,z),Quaternion(ox,oy,oz,ow))
		#print(savelocations)

if __name__ == '__main__':
        
        itchat.auto_login()
        itchat.run()  



	


