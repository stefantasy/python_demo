#coding=utf8
import os,sys
sys.path.remove('/opt/ros/kinetic/lib/python2.7/dist-packages')
import cv2
import pyzbar.pyzbar as pyzbar
from PIL import Image,ImageEnhance
cap = cv2.VideoCapture(0)

while True :
    ret, frame = cap.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    cv2.imwrite('qr.jpg',gray)
    image ="qr.jpg"
    img = Image.open(image)
    #img = ImageEnhance.Brightness(img).enhance(2.0)#增加亮度

    #img = ImageEnhance.Sharpness(img).enhance(17.0)#锐利化

    #img = ImageEnhance.Contrast(img).enhance(4.0)#增加对比度

    #img = img.convert('L')#灰度化

    barcodes = pyzbar.decode(img)
    for barcode in barcodes:
        barcodeData = barcode.data.decode('utf-8')
        print(barcodeData)
    os.system('rm qr.jpg')
    
